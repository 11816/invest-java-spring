<%@page pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="icon" href="/favicon.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="/css/bootstrap.css">
        <link rel="stylesheet" href="/css/style.css">
	</head>
	<body>
		<header>
			<h1>Biblioteka</h1>
			<a href="/">Knygos</a>
			<a href="/authors">Autoriai</a>
			<a href="/genres">Žanrai</a>
		</header>
		<main>
			<section>
				<h2>Žanrai</h2>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<th>Pavadinimas</th>
					</thead>
					<tbody>
					    <c:forEach items="${genres}" var="genre">
                            <tr>
                                <td>${genre.name}</td>
                            </tr>
                        </c:forEach>
					</tbody>
				</table>
			</section>
		</main>
	</body>
</html>
