<%@page pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8" />
		<link rel="icon" href="/favicon.png" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="/css/bootstrap.css">
		<link rel="stylesheet" href="/css/style.css">
	</head>
	<body>
		<header>
			<h1>Biblioteka</h1>
			<a href="/">Knygos</a>
			<a href="/authors">Autoriai</a>
			<a href="/genres">Žanrai</a>
		</header>
		<main>
			<section>
				<h2>Knygos</h2>
				<table class="table table-striped table-bordered table-hover">
					<thead>
						<th>Pavadinimas</th>
						<th>Autoriai</th>
						<th>Leidimo data</th>
					</thead>
					<tbody>
						<tr>
							<td>Foo</td>
							<td>Vardenis</td>
							<td>2022</td>
						</tr>
					</tbody>
				</table>
			</section>
		</main>
	</body>
</html>
