package lt.invest.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping
public class MainController {
	@Autowired
	GenreRepository repository;

	@GetMapping
	public String viewBooks(Model model) {
		return "index";
	}

	@GetMapping("/genres")
	public String getGenres(Model model) {
		model.addAttribute("genres", repository.findAll());
		return "genres";
	}

	@GetMapping("/authors")
	public String getAuthors(Model model) {
		return "authors";
	}
}
